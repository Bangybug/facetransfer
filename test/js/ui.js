/**
* @author Bangybug https://vk.com/bug_dog
*/

var APP = (function(APP, $)
{
    if (typeof APP === 'undefined') APP = {};
    if (typeof APP.UI === 'undefined') APP.UI = {};

    var $div;
    var renderer, scene, camera, controls, technique, composer;
    var shouldRender, suspended;

    var frameMeshes = [];
    var frameNumber = 0;

    var ANIM_PLAYING = 1,
        ANIM_STOPPED = 2;
    var animState = ANIM_STOPPED;
    var animSpeed = 100; // miliseconds per frame
    var nextFrame = 0;
    var maxFrames = 0; // leave zero if you want full frame animation

    var photoTexture;
    var photoWidth = 800;
    var photoHeight = 1000;

    function init()
    {
        $div = $('#model-render');
        $div.show();

        renderer = new THREE.WebGLRenderer( { antialias: true } );
        $div.get(0).appendChild( renderer.domElement );

        photoTexture = new THREE.TextureLoader().load( '../input/photo.jpg', function(texture){
            shouldRender = true;
        } );

        renderer.autoClear = true;
        renderer.sortObjects = false;
        renderer.clear();

        scene = new THREE.Scene();
        scene.background = new THREE.Color( 0 );

        camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 0.1, 5000 );
        camera.position.z = 700;

        // var aspect = 0.75;
        // camera = new THREE.OrthographicCamera( frustumSize * aspect / - 2, frustumSize * aspect / 2, frustumSize / 2, frustumSize / - 2, 1, 2000 );
        // camera.position.x = 0;
        // camera.position.y = -0.3;
        // camera.position.z = 12;
        //camera.lookAt( scene.position )

        scene.add(camera);
        
        window.addEventListener( 'resize', __onWindowResize, false );

        controls = new THREE.TrackballControls( camera, $div.get(0) );
        controls.minDistance = 0.3;
        controls.maxDistance = 700;
        controls.rotateSpeed = 1.0;
        controls.zoomSpeed = 1.2;
        controls.panSpeed = 0.8;
        controls.noZoom = false;
        controls.noPan = true;
        controls.staticMoving = true;
        controls.dynamicDampingFactor = 0.3;
        controls.keys = [ 65, 83, 68 ];
        controls.addEventListener( 'change', function(){ 
            shouldRender = true; 
        } );

        technique = simpleTechnique();
        technique.init();

        $.getJSON( "../input/frames.json", function( framesJson ) {
            create(framesJson);
            animState = ANIM_PLAYING;
            shouldRender = true;
        });

        __onWindowResize();

        animate();
    }

    function simpleTechnique() {
        var ambientLight, pointLight, material;
        return {
            init: function()
            {                
            },

            createMaterial: function()
            {
                return material || (material = new THREE.MeshBasicMaterial( { map: photoTexture } ));
            },

            setLights: function()
            {
                if (!ambientLight)
                {
                    ambientLight = new THREE.AmbientLight( 0xcccccc, 0.4 );
                    scene.add( ambientLight );
                    pointLight = new THREE.PointLight( 0xffffff, 0.9 );
                    camera.add( pointLight );
                }
            },

            onResize: function(w,h) 
            {
            }

        }
    }

    function create(framesJson)
    {
        var material = technique.createMaterial();
        material.side = THREE.DoubleSide;
        material.wireframe = false;

        technique.setLights();
        scene.add( camera );

        var uvFixed = [];

        for (var i=0; i<framesJson.length; ++i)
        {
            var frameJson = framesJson[i];
            var mesh = new THREE.Mesh(
                new MyBufferGeometry(frameJson, uvFixed),
                material
            );
            mesh.visible = false;
            frameMeshes.push(mesh);
            scene.add( mesh );
        }

        frameNumber = 0;
    }

    function scheduleNextFrame()
    {
        nextFrame = Date.now() + animSpeed;
        var prevFrame = frameNumber++;

        var animEndFrame = Math.min(maxFrames || frameMeshes.length, frameMeshes.length);

        if (frameNumber >= animEndFrame)
            frameNumber = 0;
        
        if (frameMeshes[prevFrame])
            frameMeshes[prevFrame].visible = false;
        if (frameMeshes[frameNumber])
            frameMeshes[frameNumber].visible = true;

        shouldRender = true;
    }

    function __onWindowResize()
    {
        var $screen = $div;
        var w = $screen.width(),
            h = $screen.height();
        
        camera.aspect = w/h;
     
        camera.updateProjectionMatrix();
        shouldRender = true;
        renderer.setSize( w, h );

        if (composer)
            composer.setSize( w, h );

        technique.onResize(w,h);

        if (controls)
            controls.handleResize();
    }


    function animate() 
    {
        requestAnimationFrame( animate, renderer.domElement );
        if (!suspended && controls)
            controls.update();
        if (shouldRender)
            render();

        if (animState === ANIM_PLAYING)
        {  
            if (Date.now() > nextFrame)
                scheduleNextFrame();
        }
    }

    function render()
    {
        shouldRender = false;
        renderer.render( scene, camera );
    }


    function MyBufferGeometry( frameJson, uvFixed ) 
    {
        THREE.BufferGeometry.call( this );

        this.parameters = {
            frameJson: frameJson,
            uvFixed: uvFixed
        };
        
        this.type = 'MyBufferGeometry';

        var indices = [];
        var vertices = [];
        var normals = [];
        var origin = [photoWidth/2, photoHeight/2];

        var shouldPopulateUv = !uvFixed.length;

        for (var i=0; i<frameJson.length; ++i)
        {
            var tri = frameJson[i];
            for (var j=0; j<tri.length; ++j)
            {
                var xy = tri[j].split(" ").map(function(s){return parseInt(s) });
                vertices.push( 
                    xy[0] - origin[0], 
                    -xy[1] + origin[1], 
                    0);
                normals.push(0,0,1);

                if (shouldPopulateUv)
                {
                    uvFixed.push( xy[0] / photoWidth );
                    uvFixed.push( 1 - xy[1] / photoHeight  );
                }
            }
            var q = indices.length;
            indices.push(q, q+1, q+2);           
        }

        this.setIndex( indices );
        this.addAttribute("position", new THREE.Float32BufferAttribute( vertices, 3 ));
        this.addAttribute("normal", new THREE.Float32BufferAttribute( normals, 3 ));
        this.addAttribute("uv", new THREE.Float32BufferAttribute( uvFixed, 2 ));
    }

    MyBufferGeometry.prototype = Object.create( THREE.BufferGeometry.prototype );
    MyBufferGeometry.prototype.constructor = MyBufferGeometry;
 
    $(document).ready(function(){
        init();
    });

    return APP;
})(APP, jQuery);
